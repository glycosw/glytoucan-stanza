class UserEntryStanza < TogoStanza::Stanza::Base
  property :entry do |uid|
    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
      PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      PREFIX owl: <http://www.w3.org/2002/07/owl#>
      PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
      PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
      PREFIX foaf: <http://xmlns.com/foaf/0.1/>
      PREFIX dcterms:  <http://purl.org/dc/terms/>

      SELECT DISTINCT ?userID ?userName ?project ?dbID
      FROM <http://purl.jp/bio/12/glyco/glycan#>
      FROM <http://rdf.glytoucan.org/users>
      WHERE{
        ?user a foaf:Person .
        VALUES ?userID {#{uid}}
        ?user dcterms:identifier ?userID .
        ?user foaf:name ?userName .
        OPTIONAL{
          ?user foaf:member ?db .
          ?db rdfs:label ?project .
          BIND(strafter(str(?db), 'http://purl.jp/bio/12/glyco/glycan#') AS ?dbID)
        }
      }
    SPARQL
  end

  property :database do
    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
      PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      PREFIX owl: <http://www.w3.org/2002/07/owl#>
      PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
      PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
      PREFIX foaf: <http://xmlns.com/foaf/0.1/>
      PREFIX dcterms:  <http://purl.org/dc/terms/>

      SELECT DISTINCT ?dbID ?project
      FROM <http://purl.jp/bio/12/glyco/glycan#>
      WHERE{
        ?db  a glycan:glycan_database .
        ?db rdfs:label ?label .
        BIND(str(?label) AS ?project)
        BIND(strafter(str(?db), 'http://purl.jp/bio/12/glyco/glycan#') AS ?dbID)
        ?db glycan:isAutoRegistration false .
      } ORDER BY ?label
    SPARQL
  end
end
