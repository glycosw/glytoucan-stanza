class SubstructureSearchStanza < TogoStanza::Stanza::Base

  require 'net/http'
  require 'uri'
  require 'json'

  @@prefix = [
    'PREFIX foaf: <http://xmlns.com/foaf/0.1/>',
    'PREFIX glycan:  <http://purl.jp/bio/12/glyco/glycan#>',
    'PREFIX glytoucan:  <http://www.glytoucan.org/glyco/owl/glytoucan#>'
  ].join("\n")
  @@from = [
    'FROM <http://rdf.glytoucan.org/core>'
  ].join("\n")

  @@item_labels = {}
  @@default_labels = {
    'accessionNumber' => 'Accession Number',
  }

  query = {}

  property :substructure do |wurcs,order,orderkey,offset,lang|

    seq = wurcs
    uri = URI.parse("http://rdf.glytoucan.org/glycans/sparql/substructure/")
    result = Net::HTTP.start(uri.host, uri.port) {|http|
      # http.get(uri.path + "?sequence=#{seq}&format=WURCS")
      http.get(uri.path + "?sequence=WURCS%3D#{seq}&format=WURCS%3D#{seq}")
    }
    json = result.code == '200' || result.code == '304' ? result.body : '{"status":false}'
    query = JSON.parse(json)

    uri = URI.parse("http://local.glytoucan.org/localizations/get_json/#{lang}")
    result = Net::HTTP.start(uri.host) { |http| http.get(uri.path) }
    json = result.code == '200' || result.code == '304' ? result.body : '{"status":false}'
    list = JSON.parse(json)
    lang_replace = list['status'] == false ? {} : list['result']['common']
    @@default_labels.each do |key,label|
      @@item_labels[key] = lang_replace[key].nil? ? label : lang_replace[key]
    end

    orderkey = orderkey.empty? ? 'AccessionNumber' : orderkey

    entry_list = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
      #{query['define']}
      #{query['prefix']}
      #{@@prefix}
      SELECT DISTINCT ?AccessionNumber
        WHERE {
          {
          SELECT DISTINCT ?AccessionNumber
          #{query['from']}
          #{@@from}
          WHERE {
            #{query['where']}

            # Accession Number
            ?glycan glytoucan:has_primary_id ?AccessionNumber .

          }
        ORDER BY #{order}(?#{orderkey})
        }
      }
      OFFSET #{offset}
      LIMIT 20
    SPARQL

    count_query = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
      #{query['define']}
      #{query['prefix']}
      #{@@prefix}
      SELECT (COUNT(DISTINCT ?AccessionNumber) AS ?total)
      #{query['from']}
      #{@@from}
      WHERE {
        #{query['where']}
        # Accession Number
        ?glycan glytoucan:has_primary_id ?AccessionNumber .
      }
    SPARQL

    entry_list.map{|entry|
      entry[:l_accessionNumber] = @@item_labels['accessionNumber']
      entry[:total] = count_query[0][:total]
      entry
    }

  end

end
