class GlycanEntryStanza < TogoStanza::Stanza::Base
  require 'net/http'
  require 'uri'
  require 'json'

  @@item_labels = {}
  @@default_labels = {
    'accessionNumber' => 'Accession Number',
    'mass' => 'Mass',
    'contribution' => 'Contribution',
    'structure' => 'Structure',
    'hide' => 'Hide',
    'showAll' => 'Show All'
  }

  property :entry do |acc,lang|

    uri = URI.parse("http://local.glytoucan.org/localizations/get_json/#{lang}")
    result = Net::HTTP.start(uri.host) { |http| http.get(uri.path) }
    json = result.code == '200' || result.code == '304' ? result.body : '{"status":false}'
    list = JSON.parse(json)
    lang_replace = list['status'] == false ? {} : list['result']['common']
    @@default_labels.each do |key,label|
      @@item_labels[key] = lang_replace[key].nil? ? label : lang_replace[key]
    end

    entry_content = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
    PREFIX glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#>

    SELECT DISTINCT ?WURCS_label ?GlycoCT ?Mass_label ?Contributor ?ContributionTime
    FROM <http://rdf.glytoucan.org/core>
    FROM NAMED <http://rdf.glytoucan.org/mass>
    FROM <http://rdf.glytoucan.org/sequence/wurcs>
    FROM <http://rdf.glytoucan.org/sequence/glycoct>
    FROM <http://rdf.glytoucan.org/users>
    WHERE {
      # repository RDF
      # Accession Number
      ?glycan a glycan:saccharide.
      ?glycan glytoucan:has_primary_id "#{acc}" .

      # Sequence
       # WURCS
      OPTIONAL{
      ?glycan glycan:has_glycosequence ?wcsSeq .
      ?wcsSeq rdfs:label ?WURCS_label .
      ?wcsSeq glycan:in_carbohydrate_format glycan:carbohydrate_format_wurcs .
      }
       # GlycoCT
      OPTIONAL{
      ?glycan glycan:has_glycosequence ?gctSeq .
      ?gctSeq glycan:has_sequence ?GlycoCT .
      ?gctSeq glycan:in_carbohydrate_format glycan:carbohydrate_format_glycoct .
      }

      # Mass
      OPTIONAL{
      GRAPH <http://rdf.glytoucan.org/mass>{
        ?glycan glytoucan:has_derivatized_mass ?dmass .
        ?dmass rdfs:label ?Mass_label .
      }}

      # Contributor
      OPTIONAL{
      ?glycan glycan:has_resource_entry ?res .
      ?res a glycan:resource_entry ;
           glytoucan:date_registered ?ContributionTime ;
           glytoucan:contributor ?c .
      ?c foaf:name ?Contributor .
      }
    } LIMIT 1
  SPARQL

  entry_content.map{|entry|
    mass_is_num = true if Float(entry[:Mass_label]) rescue false
    entry[:Mass_label] = (entry[:Mass_label].to_f * 10000).round / 10000.0 if mass_is_num
    entry[:l_accessionNumber] = @@item_labels['accessionNumber']
    entry[:l_mass] = @@item_labels['mass']
    entry[:l_contribution] = @@item_labels['contribution']
    entry[:l_structure] = @@item_labels['structure']
    entry[:l_hide] = @@item_labels['hide']
    entry[:l_showAll] = @@item_labels['showAll']
    entry
  }

  end

  property :check_bcsdb do |acc, lang|
    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
      PREFIX dcterms: <http://purl.org/dc/terms/>
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      PREFIX bibo: <http://purl.org/ontology/bibo/>
      PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
      PREFIX glycodb: <http://purl.jp/bio/12/database/>
      PREFIX glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#>

      SELECT (COUNT(DISTINCT ?bcsdb_id) AS ?total)
      FROM <http://rdf.glytoucan.org/core>
      FROM NAMED <http://rdf.glycoinfo.org/mapping/glytoucan/glycome-db>
      FROM NAMED <http://rdf.glycoinfo.org/glycome-db>
      FROM NAMED <http://rdf.glycoinfo.org/bcsdb>
      WHERE{
        # Glytoucan
        VALUES ?AccessionNumber {"#{acc}"}
        ?glytoucan glytoucan:has_primary_id ?AccessionNumber.


        # From glytoucan to glycome-db
        GRAPH <http://rdf.glycoinfo.org/mapping/glytoucan/glycome-db>{
          ?glytoucan skos:exactMatch ?glycomedb .
        }

        # GlycomeDB  bcsdb id
        GRAPH <http://rdf.glycoinfo.org/glycome-db>{
          ?glycomedb glycan:has_resource_entry ?res .
          ?res glycan:in_glycan_database glycan:database_bcsdb .
          ?res dcterms:identifier ?b_id.
          BIND(str(?b_id) AS ?bcsdb_id)
        }
      }
    SPARQL
  end

  property :check_glycomedb do |acc, lang|
    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
      PREFIX dcterms: <http://purl.org/dc/terms/>
      PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
      PREFIX glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#>

      SELECT (COUNT(DISTINCT ?gid) AS ?total)
      FROM <http://rdf.glytoucan.org/core>
      FROM NAMED <http://rdf.glycoinfo.org/mapping/glytoucan/glycome-db>
      WHERE{
        VALUES ?AccessionNumber {"#{acc}"}
        ?glytoucan glytoucan:has_primary_id ?AccessionNumber.

        GRAPH <http://rdf.glycoinfo.org/mapping/glytoucan/glycome-db>{
          ?glytoucan skos:exactMatch ?gdb .
          ?gdb glycan:has_resource_entry ?gurl .
          ?gurl dcterms:identifier ?gid .
        }
      }
    SPARQL
  end

  property :check_glycoepitope do |acc, lang|
    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
      DEFINE sql:select-option "order"
      PREFIX dcterms: <http://purl.org/dc/terms/>
      PREFIX bibo: <http://purl.org/ontology/bibo/>
      PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
      PREFIX glycodb: <http://purl.jp/bio/12/database/>
      PREFIX glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#>
      PREFIX glycoepitope: <http://www.glycoepitope.ac.jp/epitopes/glycoepitope.owl#>
      PREFIX glycoprot: <http://www.glycoprot.jp/>
      PREFIX uniprot: <http://www.uniprot.org/core/>

      SELECT (COUNT(DISTINCT ?ep_id) AS ?total)
      FROM <http://rdf.glytoucan.org/core>
      FROM <http://rdf.glycoinfo.org/mapping/accnum/ep>
      FROM <http://rdf.glycoinfo.org/glycoepitope>
      WHERE{
      	VALUES ?AccessionNumber {"#{acc}"}
      	?glytoucan glytoucan:has_primary_id ?AccessionNumber.

      	# epitope id
      	?glytoucan glycan:has_epitope ?ep .
      	?ep dcterms:identifier ?ep_id .
      }
    SPARQL
  end

  property :check_unicarbdb do |acc, lang|
    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
      DEFINE sql:select-option "order"
      PREFIX dcterms: <http://purl.org/dc/terms/>
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      PREFIX bibo: <http://purl.org/ontology/bibo/>
      PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
      PREFIX glycodb: <http://purl.jp/bio/12/database/>
      PREFIX glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#>

      SELECT (COUNT(DISTINCT ?unicarbdb_id) AS ?total)
      FROM <http://rdf.glytoucan.org/core>
      WHERE {

        # Glytoucan
        VALUES ?AccessionNumber {"#{acc}"}
        ?glytoucan glytoucan:has_primary_id ?AccessionNumber.
        ?glytoucan glycan:has_resource_entry ?entry .

        # UniCarb-DB
        ?entry glycan:in_glycan_database glycan:database_unicarb_db .
        ?entry dcterms:identifier ?unicarbdb_id .
        ?entry rdfs:seeAlso ?url .
      }
    SPARQL
  end

=begin
  
  property :check_unicarbkb do |acc,lang|
    uri_res = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
    PREFIX api: <http://purl.org/linked-data/api/vocab#>
    SELECT DISTINCT (str(?uri) AS ?endpoint)
    FROM <http://purl.jp/bio/12/glyco/glycan#>
    WHERE{ glycan:database_unicarbkb api:sparqlEndpoint ?uri . }
    SPARQL
    return_array = []
    uri_res.each do |ep_uri|
      sparqlEndpoint = "<#{ep_uri[:endpoint]}>"
      result = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
      PREFIX glyco: <http://purl.jp/bio/12/glyco/glycan/>
      PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
      PREFIX dcterms: <http://purl.org/dc/terms/>
      PREFIX glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#>
      SELECT (COUNT(DISTINCT ?structure_id) AS ?total)
      WHERE{
        ?glycan glytoucan:has_primary_id ?accNum FILTER(?accNum = "#{acc}")  .
        SERVICE SILENT #{sparqlEndpoint} {
            ?entry dcterms:identifier ?accNum .
            ?entry glyco:in_glycan_database glytoucan:database_glytoucan  .
            ?refcomp glyco:has_resource_entry ?entry .
            ?refcomp glyco:has_resource_entry ?kb_entry .
            ?kb_entry dcterms:identifier ?structure_id .
            ?kb_entry glyco:in_glycan_database glyco:database_unicarbkb  .
        }
      }
      SPARQL
      result.each do |res|
        return_array.push(res)
      end
    end
    return_array
  end
  
=end
 
  property :check_pubchem do |acc,lang|

    query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
    PREFIX glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#>
    PREFIX dcterms: <http://purl.org/dc/terms/>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

    SELECT (COUNT(DISTINCT ?sid) AS ?total)
    FROM <http://rdf.glytoucan.org/core>
    FROM <http://rdf.glycoinfo.org/mapping/glytoucan/pubchem>
    WHERE{
      VALUES ?AccessionNumber {"#{acc}"}
      ?saccharide glytoucan:has_primary_id ?AccessionNumber .
      ?saccharide glycan:has_resource_entry  ?cid_entry, ?sid_entry .

      ?cid_entry skos:closeMatch ?cid_url.
      ?cid_entry dcterms:identifier ?cid .

      ?sid_entry skos:exactMatch ?sid_url .
      ?sid_entry dcterms:identifier ?sid .
    }
    SPARQL
  end

end
