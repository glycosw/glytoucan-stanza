    PREFIX dcterms: <http://purl.org/dc/terms/>
    PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#> 
    PREFIX glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#>

    SELECT
    DISTINCT ?AccessionNumber
    # GlycomeDB ID
    	?url ?id
    # Carbbank(CCSD)
    	?res_ccsd ?ccsd_id
    # CFG
    	?res_cfg ?cfg_id
    FROM NAMED <http://rdf.glytoucan.org/core>
    FROM NAMED <http://rdf.glytoucan.org/sequence/wurcs>
    FROM NAMED <http://rdf.glycoinfo.org/glycome-db/sequence/wurcs>
    FROM NAMED <http://rdf.glycoinfo.org/glycome-db>
    WHERE{
    	# Glytoucan
    	GRAPH <http://rdf.glytoucan.org/core>{
    		VALUES ?AccessionNumber { }
    		?glytoucan glytoucan:has_primary_id ?AccessionNumber.
    	}

    	# Glytoucan wurcs sequence
    	GRAPH <http://rdf.glytoucan.org/sequence/wurcs>{
    		?glytoucan glycan:has_glycosequence ?gseq .
    		?gseq glycan:has_sequence ?seq .
    	}

    	# GlycomeDB ID (from wurcs sequence)
    	GRAPH <http://rdf.glycoinfo.org/glycome-db/sequence/wurcs>{
    		?gdb glycan:has_glycosequence ?gdb_gseq .
    		?gdb_gseq glycan:has_sequence ?seq .
    		BIND(STRAFTER(str(?gdb), "http://rdf.glycome-db.org/glycan/") as ?id )
    		BIND( iri(replace(str(?gdb), "http://rdf.glycome-db.org/glycan/", "http://www.glycome-db.org/database/showStructure.action?glycomeId=")) as ?url)
    	}

    	# Carbbank(CCSD)
    	OPTIONAL{
    	GRAPH <http://rdf.glycoinfo.org/glycome-db>{
    		?gdb glycan:has_resource_entry ?res_ccsd .
    		?res_ccsd glycan:in_glycan_database glycan:database_carbbank .
    		?res_ccsd dcterms:identifier ?ccsd_id.
    	}}

    	# CFG
    	OPTIONAL{
    	GRAPH <http://rdf.glycoinfo.org/glycome-db>{
    		?gdb glycan:has_resource_entry ?res_cfg .
    		?res_cfg glycan:in_glycan_database glycan:database_cfg .
    		?res_cfg dcterms:identifier ?cfg_id.
    	}}

    }
