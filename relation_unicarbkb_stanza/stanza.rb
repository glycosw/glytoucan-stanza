class RelationUnicarbkbStanza < TogoStanza::Stanza::Base

=begin
  
  require 'net/http'
  require 'uri'
  require 'json'

  @@item_labels = {}
  @@default_labels = {
    'structure_id' => 'UniCarbKB Structure'
  }

  property :unicarbkbSearch do |acc,lang|

    uri_res = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
    PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#> 
    PREFIX api: <http://purl.org/linked-data/api/vocab#>

    SELECT DISTINCT (str(?uri) AS ?endpoint)
    FROM <http://purl.jp/bio/12/glyco/glycan#>
    WHERE{
      glycan:database_unicarbkb api:sparqlEndpoint ?uri .
    } 
    SPARQL

    return_array = []

    uri_res.each do |ep_uri|

      sparqlEndpoint = "<#{ep_uri[:endpoint]}>" 

      result = query('http://test.ts.glytoucan.org/sparql', <<-SPARQL.strip_heredoc)
      PREFIX glyco: <http://purl.jp/bio/12/glyco/glycan/>
      PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>
      PREFIX dcterms: <http://purl.org/dc/terms/>
      PREFIX glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#> 
      
      SELECT DISTINCT  ?structure_id ?url
      WHERE{
        ?glycan glytoucan:has_primary_id ?accNum FILTER(?accNum = "#{acc}")  .
        SERVICE SILENT #{sparqlEndpoint} {
            ?entry dcterms:identifier ?accNum .
            ?entry glyco:in_glycan_database glytoucan:database_glytoucan  .
            ?refcomp glyco:has_resource_entry ?entry .
            ?refcomp glyco:has_resource_entry ?kb_entry .
            ?kb_entry dcterms:identifier ?structure_id .
            ?kb_entry glyco:in_glycan_database glyco:database_unicarbkb  .
        }
        BIND(CONCAT("http://www.unicarbkb.org/structure/", ?structure_id) AS ?url)
      }
      SPARQL

      result.each do |res|
        return_array.push(res)
      end

    end
    return_array

  end

  property :unicarbkbLabels do |acc,lang|

    uri = URI.parse("http://local.glytoucan.org/localizations/get_json/#{lang}")
    result = Net::HTTP.start(uri.host) { |http| http.get(uri.path) }
    json = result.code == '200' || result.code == '304' ? result.body : '{"status":false}'
    list = JSON.parse(json)
    lang_replace = list['status'] == false ? {} : list['result']['common']
    @@default_labels.each do |key,label|
      @@item_labels[key] = lang_replace[key].nil? ? label : lang_replace[key]
    end

    label_list = [{}]
    label_list.map{|entry|
      entry[:l_unicarbkb_id] = @@item_labels['structure_id']
      entry
    }

  end

=end

end

